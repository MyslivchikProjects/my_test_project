from django.urls import re_path
from workers import consumers

websocket_urlpatterns = [
    re_path(r'ws/', consumers.WorkersConsumer.as_asgi())
]