import os
from celery import Celery
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ads.settings')

app = Celery('ads')
app.config_from_object('django.conf:settings', namespace="CELERY")


# Load task modules from all registered Django app configs.
app.autodiscover_tasks()

app.conf.beat_schedule = {
    'send-change-every-day': {
        'task': 'workers.tasks.change_status_publication',
        'schedule': crontab(minute=0, hour=0),
    },
    'send-deletion-every-day': {
        'task': 'workers.tasks.permanent_deletion',
        'schedule': crontab(minute=15, hour=0),
    },
}

