from django import forms

from advert.models import City, Category


# TODO: не используем reformat code фичу / или палгины для реформатирования кода.

class AdvertFormFirst(forms.Form):
    title = forms.CharField(max_length=128, label='Название')
    city = forms.ModelChoiceField(queryset=City.objects.all(), label='Город')
    main_image = forms.ImageField(label='Главное фото')
    description = forms.CharField(widget=forms.Textarea, max_length=512, label='Описание', required=False)
    category = forms.ModelChoiceField(queryset=Category.objects.all(), label='Категория')
    price = forms.FloatField(label='Цена')

    def __init__(self, *args, **kwargs):
        super(AdvertFormFirst, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'


class AdditionalImageForm(forms.Form):
    image = forms.ImageField(label='Добавить фото', widget=forms.FileInput(attrs={'multiple': 'multiple'}), required=False)

    def __init__(self, *args, **kwargs):
        super(AdditionalImageForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'


class ContactInfoForm(forms.Form):
    name = forms.CharField(max_length=16, label='Имя', required=False)
    phone = forms.RegexField(regex=r'^\+?1?\d{9,15}$', label="Телефон", required=False)
    email = forms.EmailField(max_length=128, help_text='Required', required=False)

    def __init__(self, *args, **kwargs):
        super(ContactInfoForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'