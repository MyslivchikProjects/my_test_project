from django.contrib import admin
from django.apps import apps


# TODO: круто, но очень шаблонно. Админка будет болью для менеджмента админами
#  - неоптимальные и огромные выпадающие списки
#  - падения при поиске в списках
#  - падения по памяти
#  - проблемы с БД из-за огромных выгрузок
# Register your models here.
for model in apps.get_app_config('advert').models.values():
    admin.site.register(model)