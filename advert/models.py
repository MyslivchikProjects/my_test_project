from django.contrib.auth.models import User
from django.db import models

# Create your models here.

class City(models.Model):
    # TODO: не уникален
    title = models.CharField(max_length=128, verbose_name='title', blank=True)

    class Meta:
        verbose_name = 'Город'
        verbose_name_plural = 'Города'

    def __str__(self):
        return self.title


class Category(models.Model):
    # TODO: не уникальна
    title = models.CharField(max_length=128, verbose_name='title', blank=True)

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def __str__(self):
        return self.title


class Advert(models.Model):
    created_user = models.ForeignKey(User, on_delete=models.CASCADE)
    date_create = models.DateField(auto_now_add=True)
    published = models.BooleanField(default=True)
    date_delete = models.DateField(blank=True, null=True)
    title = models.CharField(max_length=128, verbose_name='title', blank=True)
    city = models.ForeignKey(City, on_delete=models.PROTECT)
    category = models.ForeignKey(Category, on_delete=models.PROTECT)
    description = models.CharField(max_length=512, verbose_name='description', blank=True)
    main_image = models.ImageField(upload_to='images/')
    price = models.FloatField(verbose_name='price')

    # TODO: editable=False
    count_views = models.IntegerField(default=0)

    class Meta:
        verbose_name = 'Объявление'
        verbose_name_plural = 'Объявления'


class AdditionalImage(models.Model):
    # TODO: related_name='images' было бы лучше чем магия `additionalimage_set`
    advert = models.ForeignKey(Advert, on_delete=models.CASCADE,
                               related_name='images')
    image = models.ImageField(upload_to='images/')


class ContactInfo(models.Model):
    # TODO: related_name='contacts' было бы лучше чем магия `contactinfo_set`
    advert = models.ForeignKey(Advert, on_delete=models.CASCADE)
    name = models.CharField(max_length=16, verbose_name='name', blank=True)
    phone = models.CharField(max_length=16, verbose_name='phone', blank=True)
    email = models.EmailField(max_length=32, blank=True)