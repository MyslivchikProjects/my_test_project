import datetime
from django.core.cache import cache
from django.db.models import Prefetch, Q
from django.shortcuts import render, redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from advert.form import AdvertFormFirst, AdditionalImageForm, ContactInfoForm
from advert.models import Advert, AdditionalImage, ContactInfo, City


# TODO: глобально: валидация на уровне форм, не кастомно if/else,
#  и бещ приведения типов. мне кажется мы с легккостью получим 500 ошибки
#  здесь на том же int(GET['min']), так как юные хакеры передадут вместо
#  числа букву

def private_office(request):
    return render(request, 'advert/private_office.html', {'user': request.user})


def adverts_list(request):
    pull_off = request.GET.get('pull_off', False)
    key_city = 'city_id' if request.GET.get('city') else 'city__id__regex'
    key_user = 'created_user_id' if request.GET.get('created_user_id') else 'created_user__id__regex'
    cities = City.objects.all() if request.GET.get('created_user_id') else None
    filters = {
        'published': False if request.GET.get('published') else True,
        'price__gte': int(request.GET.get('min')) if request.GET.get('min') else 0,
        'price__lte': int(request.GET.get('max')) if request.GET.get('min') else 100_000_000, # TODO: Java way for long numbers 1_000_000
        key_city: request.GET.get('city') if request.GET.get('city') else r'\d',
        key_user: request.GET.get('created_user_id') if request.GET.get('created_user_id') else r'\d',
        'date_delete__isnull': False if request.GET.get('date_delete__isnull') else True,
    }
    search_filters = Q()
    if request.GET.get('query_search'):
        search_filters |= Q(title__icontains=request.GET.get('query_search')) | Q(
            description__icontains=request.GET.get('query_search'))
    if pull_off:
        date_now = datetime.datetime.today().date()
        date_pull_off = date_now - datetime.timedelta(days=10)
        filters['date_create__gt'] = date_pull_off
    adverts_list = Advert.objects.filter(search_filters, **filters).order_by('-date_create')
    paginator = Paginator(adverts_list, 10)
    page_number = request.GET.get('page')
    try:
        page_obj = paginator.get_page(page_number)
    except PageNotAnInteger:
        page_obj = paginator.get_page(1)
    except EmptyPage:
        page_obj = paginator.get_page(paginator.num_pages)
    return render(request, 'advert/adverts_list.html',
                  {'page_obj': page_obj, 'user_advert': request.GET.get('created_user_id'), 'cities': cities})


def add_advert(request):
    # TODO: разбил бы на разные view, можно не трекать шаги в кэше, лучше в сессии.
    #  мы по сути забираем чужие шаги и перезатираем этот ключ. 10 юзеров
    #  будут все работать с одним шагом и будет неразбериха.
    #  я бы после создания редиректил на следующий шаг по ID
    step = cache.get('step', 1)
    if request.method == 'POST':
        if step == 1:
            form = AdvertFormFirst(request.POST, request.FILES)
            if form.is_valid():
                data_form = form.cleaned_data
                new_advert = Advert(created_user=request.user, title=data_form['title'],
                                    city=data_form['city'], category=data_form['category'],
                                    description=data_form['description'], main_image=data_form['main_image'],
                                    price=data_form['price'])
                new_advert.save()
                form_second = AdditionalImageForm()
                step += 1
                cache.set('advert', new_advert.id)
                cache.set('step', step)
                return render(request, 'advert/add_advert.html', {'form': form_second, 'step': step})
        elif step == 2:
            form = AdditionalImageForm(request.POST, request.FILES)
            if form.is_valid():
                images = request.FILES.getlist('image')
                for img in images:
                    add_images = AdditionalImage(advert_id=cache.get('advert'), image=img)
                    add_images.save()
                form_third = ContactInfoForm(initial={'name': '%s %s %s' % (
                request.user.first_name, request.user.last_name, request.user.profile.patronymic),
                                                      'phone': '%s' % request.user.profile.phone,
                                                      'email': '%s' % request.user.email})
                step += 1
                cache.set('step', step)
                return render(request, 'advert/add_advert.html',
                              {'form': form_third, 'step': step})
        else:
            form = ContactInfoForm(request.POST)
            if form.is_valid():
                data_form = form.cleaned_data
                if data_form['name'] or data_form['phone'] or data_form['email']:
                    contact_info = ContactInfo(name=data_form['name'], phone=data_form['phone'],
                                               email=data_form['email'])
                    contact_info.save()
                advert_id = cache.get('advert')
                cache.delete('step')
                cache.delete('advert')
                # TODO: хардкодинг url плохо способ, нужно использовать reverse.
                #  пример проблемы: спустя время поменялся url path и надо
                #  исккать и менять их в коде, используя reverse, мы даем
                #  имя url и генеим path по имени.
                url = reverse('detail_advert')
                return HttpResponseRedirect(url, {'advert_id': advert_id})
    else:
        form = AdvertFormFirst()
    return render(request, 'advert/add_advert.html', {'form': form, 'step': step})


def del_advert(request):
    # TODO: нет проверки прав на объект, мы по сути перебором
    #  можем удалить все чужие записи из БД
    Advert.objects.filter(id=request.GET.get('advert_id')).update(date_delete=datetime.datetime.today().date())
    # TODO: reverse
    return redirect('../adverts_list?created_user_id=%s&date_delete__isnull=True' % request.user.id)


def reestablish_advert(request):
    # TODO: нет проверки прав на объект, мы по сути перебором
    #  можем откатить все чужие записи из БД
    Advert.objects.filter(id=request.GET.get('advert_id')).update(date_delete=None)
    # TODO: reverse
    return redirect('../adverts_list?created_user_id=%s&date_delete__isnull=True' % request.user.id)


@transaction.atomic
def detail_advert(request):
    advert_id = request.GET.get('advert_id', '')

    # TODO: плохой тон обрамлять все в try/except. В идеале мы должны найти
    #  потенциальную точку ошибки и только ее обернуть.
    try:
        detail_advert = Advert.objects.filter(id=advert_id).prefetch_related(
            Prefetch('additionalimage_set', to_attr='additional_image'),
            Prefetch('contactinfo_set', to_attr='contact_info'))
        # TODO: добавляем магию на уровнеь JS. По сути мы думаем за то,
        #  как на JS выглядит boolean и пытаемся из питона передать строку,
        #  которая при выводе в шаблоне чудом окажется true/false.
        #  Лучше использовать django templates if/else и реализовать логику на
        #  шаблонизаторе, вместо магическиз перобразований в JS types.
        removed_public = 'true' if detail_advert.get().published else 'false'
        day_removed_public = (detail_advert.get().date_create - (datetime.datetime.today().date() - datetime.timedelta(days=10))).days if detail_advert.get().published and not detail_advert.get().date_delete else 0
        if detail_advert.get().created_user != request.user:
            counter = 1
            # TODO: счетчик будет показывать не всегда правильное значение,
            #  может возникнуть условие гонки.
            counter += detail_advert.select_for_update().get().count_views
            detail_advert.update(count_views=counter)
        return render(request, 'advert/detail_advert.html', {'detail_advert': detail_advert, 'removed_public': removed_public, 'day_removed_public': day_removed_public})
    except:
        return redirect('../adverts_list?created_user_id=%s' % request.user.id)
