from rest_framework import routers
from django.conf import settings
from django.conf.urls.static import static
from django.urls import include, path, re_path as url
from .views import *
# TODO: плохо, так как загружает все импорты + view  функции. тяжело

router = routers.DefaultRouter()

urlpatterns = [
    url(r'^', include(router.urls)),
    path('private_office/', private_office, name='private_office'),
    path('adverts_list/', adverts_list, name='adverts_list'),
    path('add_advert/', add_advert, name='add_advert'),
    path('del_advert/', del_advert, name='del_advert'),
    path('reestablish_advert/', reestablish_advert, name='reestablish_advert'),
    path('detail_advert/', detail_advert, name='detail_advert'),
]
urlpatterns += static(settings.MEDIA_URL,
                              document_root=settings.MEDIA_ROOT)