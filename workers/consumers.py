from asgiref.sync import async_to_sync
from channels.exceptions import DenyConnection
from channels.generic.websocket import WebsocketConsumer
from django.contrib.auth.models import AnonymousUser
import json


class WorkersConsumer(WebsocketConsumer):
    def connect(self):
        self.room_group_name = 'noty'
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )
        if self.scope['user'] == AnonymousUser():
            raise DenyConnection("Необходима аутентификация")

        self.accept()


    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']
        type_mes = text_data_json['type_mes']
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            {
                'type': 'send_notification',
                'type_mes': type_mes,
                'message': message
            }
        )


    def send_notification(self, text_data):
        data_out = {
            'count': 'Количество просмотров Вашего объявления превысило 10 000',
            'removed_public': 'Ваше объявления снято с публикации',
            'removed_soon': 'Снятие вашего объявления с публикации через %s дней' % text_data['message']
        }
        type_mes = text_data['type_mes']
        message = data_out[type_mes]
        self.send(text_data=json.dumps({
            'message': message
        }))

    def disconnect(self, close_code):
        print("Closed websocket with code: ", close_code)
        async_to_sync(self.channel_layer.group_discard)(
            'events',
            self.channel_name
        )
        self.close()
