from rest_framework import routers
from django.conf import settings
from django.conf.urls.static import static
from django.urls import include, path, re_path as url
from .views import *

router = routers.DefaultRouter()

urlpatterns = [
    url(r'^', include(router.urls)),
]
