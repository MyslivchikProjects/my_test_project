import datetime
# from ads.celery import app
from celery import shared_task
from advert.models import Advert


@shared_task()
def change_status_publication():
    date_now = datetime.datetime.today().date()
    date_public_off = date_now - datetime.timedelta(days=10)
    Advert.objects.filter(date_create__lte=date_public_off, published=True).update(published=False)
    return 'successful check_plan_new'


@shared_task()
def permanent_deletion():
    date_now = datetime.datetime.today().date()
    date_per_del = date_now - datetime.timedelta(days=10)
    Advert.objects.filter(date_delete__lte=date_per_del).delete()
    return 'successful permanent_deletion'
