import re

import six
from coreapi.compat import force_text
from django.contrib.auth import get_user_model, authenticate, login
from django.contrib.auth.models import User
from django.contrib.auth.tokens import PasswordResetTokenGenerator, default_token_generator
from django.contrib.sites.shortcuts import get_current_site
from django.shortcuts import render, redirect
from django.core.mail import EmailMessage
from django.http import HttpResponse
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes, force_str
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from register.form import RegForm, LoginForm


# from token import account_activation_token


class TokenGenerator(PasswordResetTokenGenerator):
    def _make_hash_value(self, user, timestamp):
        return(
            six.text_type(user.pk) + six.text_type(timestamp) +
            six.text_type(user.is_active)
        )
account_activation_token = TokenGenerator()


def register(request):
    if request.method == 'POST':
        form = RegForm(request.POST)
        if form.is_valid():
            data_form = form.cleaned_data
            username = data_form['email'].split('@')[0]
            user = User.objects.create_user(username=username, email=data_form['email'], password=data_form['password1'])
            user.is_active = False
            full_name = re.split(' +', data_form['full_name'])

            user.last_name = full_name[0] if full_name.__len__() else ''
            str(user)
            user.last_name = full_name[0] if len(full_name) else ''
            user.last_name = full_name[0] if len(full_name) else ''

            user.first_name = full_name[1] if full_name.__len__() > 1 else ''
            user.profile.patronymic = full_name[2] if full_name.__len__() > 3 else ''
            user.profile.phone = data_form['phone']
            user.save()
            # to get the domain of the current site
            current_site = get_current_site(request)
            mail_subject = 'Ссылка для активации отправлена на ваш адрес электронной почты'
            message = render_to_string('account/message_active_email.html', {
                'user': user,
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': account_activation_token.make_token(user),
            })
            to_email = form.cleaned_data.get('email')
            email = EmailMessage(
                        mail_subject, message, to=[to_email]
            )
            # TODO: опасное место, задержка от SMTP сервера может достигать ка долю секунжы, так и 20 секунд.
            email.send()
            return HttpResponse('Пожалуйста, подтвердите свой адрес электронной почты, чтобы завершить регистрацию')
    else:
        form = RegForm()
    return render(request, 'account/registration.html', {'form': form})


def activate(request, uidb64, token):
    User = get_user_model()
    error = False
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
    else:
        error = True
    return render(request, 'account/registration_done.html',
                  {'error': error, 'domain': get_current_site(request).domain})

def login_user(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            data_form = form.cleaned_data
            user = User.objects.get(email__exact=data_form['email'])
            if user:
                auth_user = authenticate(username=user.username, password=data_form['password'])
                if auth_user is not None:
                    if auth_user.is_active:
                        login(request, auth_user)
                        return redirect('../advert/private_office')
                    else:
                        return render(request, 'account/login.html', {'form': form, 'error': 'Акккаунт неактивен', 'domain': get_current_site(request).domain})
                else:
                    return render(request, 'account/login.html', {'form': form, 'error': 'Неверный email или пароль', 'domain': get_current_site(request).domain})
            else:
                return render(request, 'account/login.html', {'form': form, 'error': 'Email не найден', 'domain': get_current_site(request).domain})
    else:
        form = LoginForm()
    return render(request, 'account/login.html', {'form': form, 'domain': get_current_site(request).domain})


