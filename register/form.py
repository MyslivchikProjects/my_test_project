from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class RegForm(UserCreationForm):
    full_name = forms.CharField(max_length=512, label="ФИО")
    email = forms.EmailField(max_length=128, help_text='Required')
    phone = forms.RegexField(regex=r'^\+?1?\d{9,15}$', label="Телефон")

    class Meta:
        model = User
        fields = ('full_name', 'email', 'phone', 'password1', 'password2')

    def __init__(self, *args, **kwargs):
        super(RegForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'


class LoginForm(forms.Form):
    email = forms.EmailField(max_length=128)
    password = forms.CharField(widget=forms.PasswordInput)

    def __init__(self, *args, **kwargs):
        # TODO: python2 super vs python 3
        # TODO: я бы сделал миксином, так как везде пишем одинаковый код
        # super(LoginForm, self).__init__(*args, **kwargs)  # v2
        super().__init__(*args, **kwargs)  # v3
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'
