from django.db import models

# Create your models here.
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import User


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    patronymic = models.CharField(max_length=16, verbose_name='patronymic', blank=True)
    phone = models.CharField(max_length=16, verbose_name='phone')

    # TODO: плохой вараинт, мы всегда можем случайно вызвать методы у класса
    @receiver(post_save, sender=User)
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)

    @receiver(post_save, sender=User)
    @staticmethod
    def save_user_profile1(sender, instance, **kwargs):
        instance.profile.save()

    def save_user_profile2(self, instance, **kwargs):
        instance.profile.save()
