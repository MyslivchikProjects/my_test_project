from django.contrib.auth.views import PasswordResetConfirmView, PasswordResetCompleteView, PasswordResetView, PasswordResetDoneView
from rest_framework import routers
from django.urls import include, path, re_path as url
from .views import *

router = routers.DefaultRouter()
urlpatterns = [
    url(r'^', include(router.urls)),
    path('reg_user/', register, name='register'),
    path('activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/',
         activate, name='activate'),
    url(r'login_user', login_user, name='login_user'),
    url(r'password_reset/$', PasswordResetView.as_view(template_name='account/reset_password.html'), name='password_reset'),
    url(r'^password_reset/done/$', PasswordResetDoneView.as_view(template_name='account/password_reset_done.html'), name='password_reset_done'),
    path('reset/<uidb64>/<token>/',
         PasswordResetConfirmView.as_view(template_name='account/password_reset_confirm.html'),
         name='password_reset_confirm'),
    path('reset/done/',
         PasswordResetCompleteView.as_view(template_name='account/reset_complete.html'),
         name='password_reset_complete'),
]
